package ru.t1.skasabov;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.t1.skasabov.translator.service.TranslateService;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

@QuarkusTest
public class TranslateResourceTest {

    @BeforeEach
    public void init() {
        TranslateService.isWriteDatabase = false;
    }

    @Test
    public void testTranslateEndpoint() {
        given()
                .body("{\"text\":\"test\",\"language\":\"Русский\",\"address\":\"79.139.200.109\"}")
                .header("Content-Type", "application/json")
                .when()
                .post("/translate")
                .then()
                .statusCode(200)
                .body(containsString("{\"text\":\"тест\",\"errors\":[]}"));
    }

    @Test
    public void testTranslateNoText() {
        given()
                .body("{\"text\":\"\",\"language\":\"Русский\",\"address\":\"79.139.200.109\"}")
                .header("Content-Type", "application/json")
                .when()
                .post("/translate")
                .then()
                .statusCode(200)
                .body(containsString("{\"text\":\"\",\"errors\":[\"Входная строка не заполнена!\"]}"));
    }

    @Test
    public void testTranslateNoLanguage() {
        given()
                .body("{\"text\":\"test\",\"language\":\"\",\"address\":\"79.139.200.109\"}")
                .header("Content-Type", "application/json")
                .when()
                .post("/translate")
                .then()
                .statusCode(200)
                .body(containsString("{\"text\":\"\",\"errors\":[\"Язык не указан!\"]}"));
    }

}