package ru.t1.skasabov.translator;

import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.translator.api.service.ITranslateService;
import ru.t1.skasabov.translator.dto.TranslateRequest;
import ru.t1.skasabov.translator.dto.TranslateResponse;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;

@Path("/translate")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TranslateResource {

    @NotNull
    @Inject
    ITranslateService service;

    @POST
    public TranslateResponse translate(final TranslateRequest translateRequest) throws IOException {
        @NotNull final String text = translateRequest.getText();
        @NotNull final String language = translateRequest.getLanguage();
        @NotNull final String address = translateRequest.getAddress();
        return service.translate(text, language, address);
    }

}