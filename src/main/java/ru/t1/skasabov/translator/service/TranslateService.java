package ru.t1.skasabov.translator.service;

import com.darkprograms.speech.translator.GoogleTranslate;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.translator.api.service.ITranslateService;
import ru.t1.skasabov.translator.dto.TranslateRequest;
import ru.t1.skasabov.translator.dto.TranslateResponse;
import ru.t1.skasabov.translator.model.LanguageName;
import ru.t1.skasabov.translator.model.TranslatedWord;
import ru.t1.skasabov.translator.model.TranslatedText;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@ApplicationScoped
public class TranslateService implements ITranslateService {

    @NotNull
    @Inject
    EntityManager em;

    @NotNull
    @Inject
    Validator validator;

    public static boolean isWriteDatabase = true;

    @NotNull
    @Transactional
    public TranslatedText createTranslate(
            @NotNull final String inputText, @NotNull final String outputText,
            @NotNull final String language, @NotNull final String address
    ) {
        @NotNull final TranslatedText translatedText = new TranslatedText();
        translatedText.setInputText(inputText);
        translatedText.setOutputText(outputText);
        translatedText.setLanguage(language);
        translatedText.setAddress(address);
        em.persist(translatedText);
        return translatedText;
    }

    @Transactional
    public void createTranslateWord(
            @NotNull final String inputWord,
            @NotNull final String outputWord,
            @NotNull final TranslatedText translatedText
    ) {
        @NotNull final TranslatedWord translatedWord = new TranslatedWord();
        translatedWord.setInputWord(inputWord);
        translatedWord.setOutputWord(outputWord);
        translatedWord.setTranslatedText(translatedText);
        em.persist(translatedWord);
    }

    @NotNull
    private String getLanguageCode(@NotNull final String language) {
        return em.createQuery("SELECT l FROM language_name l WHERE l.langName = ?1", LanguageName.class)
                .setParameter(1, language)
                .getResultList().get(0).getLangCode();
    }

    @NotNull
    private Set<String> validate(
            @NotNull final String text,
            @NotNull final String language,
            @NotNull final String address
    ) {
        @NotNull final TranslateRequest translateRequest = new TranslateRequest();
        translateRequest.setText(text);
        translateRequest.setLanguage(language);
        translateRequest.setAddress(address);
        @NotNull final Set<ConstraintViolation<TranslateRequest>> violations = validator.validate(translateRequest);
        if (violations.isEmpty()) return new HashSet<>();
        return violations.stream().map(ConstraintViolation::getMessage).collect(Collectors.toSet());
    }

    @NotNull
    @Override
    public TranslateResponse translate(
            @NotNull final String text,
            @NotNull final String language,
            @NotNull final String address
    ) throws IOException {
        @NotNull final TranslateResponse data = new TranslateResponse();
        @NotNull final Set<String> errors = validate(text, language, address);
        data.setText("");
        data.setErrors(errors);
        if (!errors.isEmpty()) return data;
        @NotNull TranslatedText translatedText = new TranslatedText();
        @NotNull final String translateText = GoogleTranslate.translate(getLanguageCode(language), text);
        data.setText(translateText);
        if (isWriteDatabase)
            translatedText = createTranslate(text, translateText, language, address);
        @NotNull final String[] words = text.split(" ");
        for (@NotNull final String word : words) {
            @NotNull final String translateWord = GoogleTranslate.translate(getLanguageCode(language), word);
            if (isWriteDatabase)
                createTranslateWord(word, translateWord, translatedText);
        }
        return data;
    }

}
