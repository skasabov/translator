package ru.t1.skasabov.translator.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.translator.dto.TranslateResponse;

import java.io.IOException;

public interface ITranslateService {

    TranslateResponse translate(@NotNull String text, @NotNull String language, @NotNull String address) throws IOException;

}
