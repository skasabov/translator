package ru.t1.skasabov.translator.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@NoArgsConstructor
public class TranslateRequest extends AbstractRequest {

    @NotNull
    @NotBlank(message="Входная строка не заполнена!")
    private String text;

    @NotNull
    @NotBlank(message="Язык не указан!")
    private String language;

    @NotNull
    private String address;

}
