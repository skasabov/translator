package ru.t1.skasabov.translator.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class TranslateResponse extends AbstractResponse {

    @NotNull
    private String text;

    @NotNull
    private Set<String> errors = new HashSet<>();

}
