package ru.t1.skasabov.translator.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Entity(name = "translated_text")
public class TranslatedText {

    @Id
    @NotNull
    @SequenceGenerator(name = "translated_text_seq", sequenceName = "translated_text_seq", allocationSize = 1)
    @GeneratedValue(generator = "translated_text_seq")
    private Long id;

    @NotNull
    @Column(updatable = false, nullable = false)
    private Date created = new Date();

    @NotNull
    @Column(name = "input_text", nullable = false)
    private String inputText;

    @NotNull
    @Column(name = "output_text", nullable = false)
    private String outputText;

    @NotNull
    @Column(name = "target_language", nullable = false)
    private String language;

    @NotNull
    @Column(name = "ip_address_client", nullable = false)
    private String address;

}
