package ru.t1.skasabov.translator.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Entity(name = "language_name")
public class LanguageName {

    @Id
    @NotNull
    private Long id;

    @NotNull
    @Column(name = "lang_name")
    private String langName;

    @NotNull
    @Column(name = "lang_code")
    private String langCode;

}
