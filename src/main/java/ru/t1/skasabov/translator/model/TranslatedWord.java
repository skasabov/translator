package ru.t1.skasabov.translator.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Entity(name = "translated_word")
public class TranslatedWord {

    @Id
    @NotNull
    @SequenceGenerator(name = "translated_word_seq", sequenceName = "translated_word_seq", allocationSize = 1)
    @GeneratedValue(generator = "translated_word_seq")
    private Long id;

    @NotNull
    @Column(name = "input_word", nullable = false)
    private String inputWord;

    @NotNull
    @Column(name = "output_word", nullable = false)
    private String outputWord;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "translated_text_id", nullable = false)
    private TranslatedText translatedText;

}
